ARG ARCH=
FROM ${ARCH}alpine:latest

ARG ARCH
RUN apk add --no-cache docker-cli docker-compose pigz

CMD ["docker-compose version"]
